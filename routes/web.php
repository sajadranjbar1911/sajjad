<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Files;
use App\Http\Controllers\FormController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ResumeController;
use App\Http\Controllers\FileController;

Route::get('/', function () {
    return view('welcome');
});

Route::get("developerName", "UserController@getDevelopName")->name('DevelopName');

Route::get("yourName/{name?}/{family?}", "UserController@getName")->name('PrintName');

Route::get("extract", "UserController@extract")->name('FileExtract');

Route::get("getFile", "UserController@getFile")->name('GetFile');

Route::get("image/{name}/{height?}/{width?}", "ImageController@downloadImage")->name('DownloadImage');

Route::get('profileForm', [FormController::class, 'showFormProfile'])->name('Profile');

Route::get('chart', "FormController@ShowChart")->name("Chart");

Route::post('showDetail', "FormController@showDetail")->name("showDetail");

Route::get('downloadChart', "FormController@downloadChart")->name("DownloadChart");

Route::get('downloadFinalScores', 'StudentsController@downloadScores')->name("FinalScore");

Route::get('home', "HomeController@index")->name('Home');

Route::get('session', function () {
    session()->flash('USER_ID', "1");

    return session()->all();
})->name('Home');
Route::get('getSession', function () {
    return session()->all();
})->name('Home');


Route::get('showUsers', 'FormController@showUser')->name('ShowUser');

Route::get('getUserInfo', 'UserController@getUserInfo')->name('Info');
Route::get('getSkill', 'UserController@getSkill')->name('Skill');


Route::middleware(['auth'])->group(function () {

    Route::prefix('file')->group(function () {

        Route::get('fileView', [FileController::class, 'showFormFile'])->name('FileForm');

        Route::post('upload', [FileController::class, 'UploadFile'])->name("UploadFile")->middleware('can:create,App\Files');

        Route::get('edit/{file}', [FileController::class, 'edit'])->name('EditFile')->middleware('can:update,file');

        Route::post('updateFile', [FileController::class, 'updateFile'])->name('UpdateFile');

        Route::get('showFile', [FileController::class, 'showAllFile'])->name('ReadAll');

        Route::get('destroy/{file}', [FileController::class, 'destroy'])->name('DeleteFile')->middleware('can:delete,file');

        Route::get('Comment/{fileId}', [FileController::class, 'comment'])->name('Comment');

        Route::post('setComment', [FileController::class, 'setComment'])->name('setComment');

        Route::get('getComments', [FileController::class, 'allComments'])->name('GetAllComments');

        Route::get('show/{file}', [FileController::class, 'show'])->name('ShowFile');

        Route::get('download/{savedFileName}', [FileController::class, 'downloadFile'])->name('Download');

        Route::get('setTag', [FileController::class, 'setTag'])->name('SetTag');//TO DO
    });

    Route::prefix('resume')->group(function () {

        Route::get('resumeForm', [ResumeController::class, 'showResumeFormView'])->name('ResumeView');

        Route::post('setResume', [ResumeController::class, 'registerResume'])->name('SetResume');

        Route::get('resumeShow /{id}', [ResumeController::class, 'resumeShow'])->name('ResumeShow');

        Route::get('singleResumeShow', [ResumeController::class, 'singleResumeShow'])->name('SingleResume');

        Route::get('downloadResumeFile', [ResumeController::class, 'downloadResumeFile'])->name('DownloadResumeFile');

    });

    Route::prefix('post')->group(function () {

        Route::get('posts/create', [PostController::class, 'create'])->name('posts.create');

        Route::get('show/{post}', function (\App\Post $post) {
            dd($post);
        })->name('Show');

        Route::get('test', function () {
            $tag = new \App\Tag;
            dump($tag->files());
        });
    });

});

Auth::routes();

Route::get(' / home', 'HomeController@index')->name('home');

Route::get('test', function () {
    $file = Files::query()->find(11);
    return ($file->comments);
});
Route::get('testQueue', [\App\Http\Controllers\HomeController::class, 'testQueue']);
