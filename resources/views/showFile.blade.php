@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br><br>
    <div class="container">

        @if(session()->exists('DELETE'))
            <div class="row justify-content-center">
                @php($transKey=session()->get('DELETE'))
                <p @if($transKey==='Delete_success')class="text-success" @else class="text-danger"@endif>
                    {{ trans("user.$transKey") }}
                </p>
            </div>
        @endif
        @if(empty($files[0]))
            <p class="row justify-content-center text-danger">
                {{ trans('user.File-Empty') }}
            </p>
        @endif

        <a href="{{ route('ReadAll') }}" class="btn btn-primary">show all
            files</a>
        <a href="{{ route('ReadAll',['has_comment']) }}" name="has_comment" class="btn btn-primary">show only files has
            comment</a>

        <table class="table table-bordered table-striped table-hover bg-white">
            <thead>
            <tr>
                <td>#</td>
                <td>FileName</td>
                <td>Description</td>
            </tr>
            </thead>

            <tbody>
            @foreach ($files as $file)
                <tr>
                    <td>{{$file->id}}</td>
                    <td>{{$file->originalFileName}}</td>
                    <td>{{$file->description}}</td>
                    <td>
                        <a class="btn btn-outline-danger"
                           href="{{ route('DeleteFile',['file'=>$file->id]) }}"
                           role="button">
                            Delete
                        </a>
                        <a class="btn  btn-outline-warning mx-3"
                           href="{{ route('EditFile',[
                           'file'=>$file->id]) }}"
                           role="button">
                            Edit
                        </a>
                        <a class="btn  btn-secondary mx-3"
                           href="{{ route('Comment',['fileId'=>$file->id]) }}"
                           role="button">
                            Comment
                        </a>
                        <button class="btn btn-primary" type="button" data-toggle="collapse"
                                data-target="#collapse{{ $file->id }}" aria-expanded="false"
                                aria-controls="collapse{{ $file->id }}">
                            Show Comment
                        </button>
                        @foreach( $file->comments as $comment )
                            <div class="collapse" id="collapse{{ $file->id }}">
                                <div class="card card-body">
                                    {{ $comment->text }}
                                </div>
                            </div>
                        @endforeach
                        <button class="btn btn-primary" type="button" data-toggle="collapse"
                                data-target="#tag" aria-expanded="false"
                                aria-controls="tag">
                            Tag
                        </button>

                        <a href="{{ route( 'ReadAll',['has_tag'] ) }}" class="btn btn-primary" name="has_tag"
                           type="button" data-toggle="collapse"
                           data-target="#tag{{ $file->id }}" aria-expanded="false"
                           aria-controls="tag{{ $file->id }}">
                            Show Tag
                        </a>

                        @foreach( $file->tags as $tag )
                            <div class="collapse" id="tag{{ $file->id }}">
                                <div class="card card-body">
                                    {{ $tag->name}}
                                    {{ $tag->pivot->priority }}
                                </div>
                            </div>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="collapse" id="tag">
            <textarea class="form-control" name="comment" placeholder="write your Tag"></textarea>
            <a class="btn  btn-secondary mx-3"
               href="{{ route('SetTag',['fileId'=>$file->id]) }}"
               role="button">Send</a>
        </div>
        <div class="row justify-content-center">
            {{ $files->links() }}
        </div>
    </div>


@endsection

