@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br>
    <br>

    <table class="table table-bordered table-striped table-hover bg-white">
        <thead>
        <tr>
            <td>FileName</td>
            <td>Comments</td>
        </tr>
        </thead>

        <tbody>
        @foreach ( $comments as $comment)
            <tr>
                <td>

                    @if(isset($comment->commentable->originalFileName))
                        <a href="{{ route('ShowFile',['file'=>$comment->commentable->id]) }}" style="color: black">
                            {{$comment->commentable->originalFileName}}
                        </a>
                    @else
                        <a href="{{ route('Show',['post'=>$comment->commentable->id]) }}" style="color: black">
                            {{ $comment->commentable->title }}
                        </a>
                    @endif

                </td>
                <td>{{$comment->text}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@endsection
