@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br>
    <br>
    <form method="post" action="{{ route('setComment',['fileId'=>$fileId]) }}">
        @csrf
        <label for="validationTextarea">Textarea</label>
        <textarea class="form-control" name="comment" placeholder="write your comment"
                  required></textarea>
        <input type="submit">
    </form>
@endsection

