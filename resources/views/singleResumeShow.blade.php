@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <div class="row justify-content-center my-3">
        <div class="col-8 justify-content-center border rounded-lg shadow-lg p-3 mb-5 rounded">
            <div class="card">
                <div class="row card-header">


                    <div class="col-4 text-left">
                        {{ ucwords($userInf['firstName']) }} {{ ucwords($userInf['lastName']) }}
                    </div>
                    @if(!empty($userInf['email']))
                        <div class="col-4 text-center">
                            {{ $userInf['email'] }}
                        </div>
                    @endif
                    @if(!empty($userInf['phone']))
                        <div class="col-4 text-right">
                            {{ $userInf['phone'] }}
                        </div>
                    @endif
                </div>
                @php
                    $skillString='';
                foreach($skills as $skill){
                $skillString .=$skill->skillName .',';
                }
                @endphp
                <div class="card-body">
                    <h5 class="card-title">Skills</h5>
                    <p class="card-text">{{ rtrim($skillString ,',')}}</p>
                    <a href="{{ route('DownloadResumeFile',['file'=>$userInf['resumeSavedFileName']]) }}"
                       class="btn btn-primary">Download Resume File</a>
                </div>
            </div>


        </div>
    </div>
@endsection
