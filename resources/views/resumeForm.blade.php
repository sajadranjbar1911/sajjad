@extends('layouts.app')
@section('title','FileForm')

@section('content')
    @if(session()->exists('SUCCESS_REGISTER'))
        <p style="color: green" class="row justify-content-center">
            @php
                $fullName=session()->get('SUCCESS_REGISTER');
            @endphp
            {{ trans('user.SUCCESS_REGISTER',['name'=>$fullName])}}
        </p>
    @endif
    @if(session()->exists('UPLOAD_ERROR'))
        <p style="color: red" class="row justify-content-center">
            {{ trans('user.UPLOAD_ERROR')}}
        </p>
    @endif
    <div class="row justify-content-center my-3">
        <div class="col-8 justify-content-center border rounded-lg shadow-lg p-3 mb-5 rounded">
            <form method="post" action="{{ route('SetResume') }}" enctype="multipart/form-data">
                @csrf
                <label class="font-weight-bolder font-italic">Personal Information :</label>
                <div class="row">
                    <div class="col">
                        <input type="text" name="firstName"
                               class="form-control @error('firstName') is-invalid @enderror"
                               placeholder="First name">
                    </div>
                    <div class="col">
                        <input type="text" name="lastName" class="form-control @error('lastName') is-invalid @enderror"
                               placeholder="Last name">
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror"
                               placeholder="Phone Number">
                        <p class="text-muted float-right">
                            Optional
                        </p>
                    </div>
                    <div class="col">
                        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror"
                               placeholder="Email">
                        <p class="text-muted float-right">
                            Optional
                        </p>
                    </div>
                </div>
                <label class="font-weight-bolder font-italic">Skills :</label>
                <div class="row m-2">
                    @foreach( $skills as $skill)
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('skill') is-invalid @enderror" type="checkbox"
                                   name="skill[]" id="inlineCheckbox1" value="@php echo $skill->skillName; @endphp">
                            <label class="form-check-label" for="inlineCheckbox1">@php
                                    echo $skill->skillName;
                                @endphp</label>
                        </div>
                    @endforeach
                </div>
                <label class="font-weight-bolder font-italic" for="customFile">Resume File :</label>
                <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input @error('file') is-invalid @enderror"
                           id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if ($errors->any())
                    <div class="mt-5 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row justify-content-center">
                    <div class="my-5">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
