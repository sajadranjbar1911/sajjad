@extends('layouts.app')
@section('title','ProfileForm')
@section('content')
    <br><br>
    <form method="post" action="{{route('showDetail')}}">
        @csrf
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)

                        <li>
                            {{ $error }}
                        </li>
                    @endforeach
                </ul>

            </div>
        @endif
        <div class="form-row justify-content-center ">
            <div class="col-4">
                <input type="text" name="firstName" class="form-control @error('firstName') is-invalid @enderror"
                       placeholder="First name">
            </div>
            <div class="col-4">
                <input type="text" name="lastName" class="form-control @error('lastName') is-invalid @enderror"
                       placeholder="Last name">
            </div>

        </div>
        @error('firstName')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        @error('lastName')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        <br><br>
        <div class="form-row justify-content-center">
            <div class="col-4">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                       placeholder="Email">
            </div>
            <div class="col-4">
                <input type="tel" name="phone" class="form-control @error('phone') is-invalid @enderror"
                       placeholder="Phone">
            </div>
        </div>
        @error('email')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        @error('phone')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        <br><br>
        <div class="form-row justify-content-center row-cols">
            <div class="col-4">
                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror "
                       placeholder="Username">
            </div>
            <div class="col-4">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                       placeholder="Password">
            </div>
        </div>
        @error('username')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        @error('password')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        <br><br>
        <div class="form-row justify-content-center">
            <button type="submit" class="btn btn-primary mb-2">Confirm identity</button>
        </div>
    </form>
    </div>
@endsection
