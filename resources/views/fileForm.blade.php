@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br><br>
    @if ($errors->any())
        <div class="mt-5 alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('UploadFile') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-4">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="form-group col-md-4 my-3">
                    <label for="inputState">#Tags</label>
                    <select id="inputState" name="tag[]" class="form-control" multiple>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row justify-content-center my-3">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">
                        @if( isset($description) && !empty($description) )
                            {{ $description }}
                        @endif
                    </textarea>
                </div>
            </div>
            <br><br>
            <div class="form-row justify-content-center">
                <button type="submit" class="btn btn-primary mb-2">UPLOAD</button>
            </div>
        </div>
    </form>
    @if( session()->exists('UPLOAD'))
        <p style="color: green" class="row justify-content-center">
            @php
                $fileName=session()->get('UPLOAD');
            @endphp
            {{ trans('user.uploadSuccessful',['name'=>$fileName])}}
        </p>
    @endif
@endsection

