@extends('layouts.app')
@section('title','trans')
@section('content')

    @foreach($resumes as $resume)
        <div class="row justify-content-center">
            <div class="card w-25 my-2">
                <div class="card-body">
                    <h5 class="card-title text-center">{{ ucwords($resume->firstName ." ". $resume->lastName) }}</h5>
                    @if(!empty($userInf['email']))
                        <p class="card-text"> {{ $resume->email }} </p>
                    @endif
                    @if(!empty($userInf['phone']))
                        <p class="card-text"> {{ $resume->phone }} </p>
                    @endif
                    <a href="{{ route('SingleResume',['userInf'=>$resume]) }}" class="btn btn-primary">Show</a>
                </div>
            </div>
        </div>
    @endforeach

@endsection
