@extends('layouts.app')
@section('title','Chart')
@section('content')

        <table class="table table-bordered table-striped table-hover bg-white">
            <thead>
            <tr>
                <td>#</td>
                <td>name</td>
                <td>username</td>
            </tr>
            </thead>

            <tbody>
            @foreach($data as $user)
                <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['username']}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
@endsection

