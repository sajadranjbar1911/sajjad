@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br>
    <br>
    <div class="card text-center">
        <div class="card-header">
            {{ $file->originalFileName }}
        </div>
        <div class="card-body">
            <h5 class="card-title">Description</h5>
            <p class="card-text">{{ $file->description }}</p>
            <a href="{{ route('Download',['savedFileName'=>$file->savedFileName]) }}" class="btn btn-primary">Download</a>
        </div>
        <div class="card-footer text-muted">
            {{ $file->created_at }}
        </div>
    </div>
@endsection
