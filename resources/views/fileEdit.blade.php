@extends('layouts.app')
@section('title','FileForm')

@section('content')
    <br><br>

    @if( session()->exists('UPDATE'))
        <p style="color: green" class="row justify-content-center">
            @php

                $transKey=session()->get('UPDATE');
            echo trans("user.$transKey");

            @endphp
        </p>
    @endif

    @if ($errors->any())
        <div class="mt-5 alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('UpdateFile',['fileId'=>$file->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <div class="row justify-content-center my-3">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">
                        @if( isset($file->description) && !empty($file->description) )
                            {{ $file->description }}
                        @endif
                    </textarea>
                </div>
            </div>
            <br><br>
            <div class="form-row justify-content-center">
                <button type="submit" class="btn btn-primary mb-2">UPDATE</button>
            </div>
        </div>

    </form>
@endsection

