@extends('layouts.app')
@section('title','ProfileForm')
@section('content')
    <table class="table table-bordered table-striped table-hover bg-white">
        <thead>
        <tr>
            <td>#</td>
            <td>name</td>
            <td>username</td>
        </tr>
        </thead>

        <tbody>
        @foreach( $users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
    {{ $users->links() }}

@endsection
