<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '  :name رمز شما تغییر کرد!',
    'sent' => 'ایمیل مربوط به تغییر رمز عبور برای شما ارسال شد!',
    'token' => 'توکن مربوط به پسورد شما نامعتبر است!',
    'user' => "ما نمیتوانیم یک کاربر با این آدرس ایمیل پیدا کنیم",
    'throttled' => 'قبل از تلاش مجدد صبر کنید.',

];
