<?php
return [
    'welcome' => 'welcome , dear :name',
    'uploadSuccessful' => 'File :name Successfully Upload',
    'SUCCESS_REGISTER' => ':name was a successful registration',
    'UPLOAD_ERROR' => 'Could not upload file',
    'Update_failed' => 'Update failed, please try again',
    'Update_success' => 'Update successful',
    'Delete_failed' => 'Delete failed, please try again',
    'Delete_success' => 'Delete successful',
    'File-Empty' => 'No File'
];
