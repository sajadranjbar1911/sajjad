<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class ExtractZipJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $filePath;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @param $fileName
     */
    public function __construct($fileName)
    {
        $this->queue = 'extract';
        $this->filePath = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        \Log::info('start job handle');
        $archive = UnifiedArchive::open($this->filePath);
        $extractSuccessful = $archive->extractFiles(storage_path("app") . "\output");
        \Log::info('end job ');
    }
}
