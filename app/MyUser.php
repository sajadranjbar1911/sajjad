<?php


namespace App;


use Illuminate\Support\Arr;

class MyUser
{
    /**
     * The table associated with the model
     *
     *
     * @var string
     */
    protected $table = "myuser";

    protected $attributes = [];

    protected $allAttributes = [];

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param $item
     * @return MyUser
     */
    public function getModel($item)
    {
        $myUser = new MyUser();

        foreach ($item as $key => $value) {
            $myUser->setAttributes($key, $value);
        }
        return $myUser;
    }

    /**
     * @param $id
     * @return MyUser
     */
    public static function find($id)
    {
        $result = \DB::table((new static())->getTable())
            ->where('id', '=', $id)
            ->get()
            ->first();

        return (new static())->getModel($result);

    }

    /**
     * @return array
     */
    public static function all()
    {
        $result = \DB::table((new static())->getTable())
            ->get();

        return (new static())->setAllAttributes();
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttributes($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function setAllAttributes($result)
    {
        foreach ($result as $item) {

            $this->allAttributes[] = $this->getModel();
        }
        return $this;
    }
}
