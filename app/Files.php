<?php


namespace App;


use App\Events\NewFileUploaded;
use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'file';

    protected $fillable = ['originalFileName', 'savedFileName', 'description', 'user_id'];

    protected $dispatchesEvents = [
        'created' => NewFileUploaded::class,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable', 'commentable_type', 'commentable_id', 'id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class,
            'taggble',
            'taggables_tag',
            'taggble_id',
            'tag_id')
            ->withPivot('priority');
    }
}
