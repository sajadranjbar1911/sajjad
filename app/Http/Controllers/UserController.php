<?php


namespace App\Http\Controllers;


use App\MyUser;
use App\Skills;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class UserController extends Controller
{

    /**
     * return developer name
     * @param Request $request
     * @return string
     */
    public function getDevelopName(Request $request)
    {
        return "sajjad ranjbar";
    }

    /**
     * @param string $name
     * @param string $family
     * @param Request $request
     * @return string
     */
    public function getName($name = "laravel", $family = "")
    {
        return "your name is : $name $family";
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function extract(Request $request)
    {

        $name = $request->input('zip_name');
        if (!file_exists(storage_path() . env('ZIP_FILES_PATH', false) . $name))
            return response()->json("This file does not exist");

        $archive = UnifiedArchive::open(storage_path() . env('ZIP_FILES_PATH', false) . $name);
        $extractSuccessful = $archive->extractFiles(storage_path("app") . "\output");

        if ($extractSuccessful) {
            return $archive->getFileNames();
        } else {
            return response()->json(false, 419);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getFile(Request $request)
    {

        $this->extract($request);

        $name = $request->input('txt_name');
        if (empty($name)) {
            $name = "index.txt";
        }
        if (!file_exists(storage_path("app\output") . "\\$name")) {
            return response()->json("This file does not exist");
        }

        $contents = Storage::get("output/" . $name);
        echo $contents;


    }

    public function routing()
    {
        dd(route('DownloadImage', ['name' => "i.jpg", 'test' => 'mytest']));
    }

    public function getUserInfo()
    {

        dd(MyUser::all());
    }

    public function getSkill()
    {
        $skill = new Skills();
        dd($skill->all());
    }
}
