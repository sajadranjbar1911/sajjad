<?php


namespace App\Http\Controllers;


use App\Http\Requests\Resume;
use Doctrine\DBAL\Schema\AbstractAsset;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use phpDocumentor\Reflection\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ResumeController
{
    /**
     * resume file directory
     */
    private $ResumeFileDirectory = 'Resume-File';

    /**
     * show resume form view
     * @return Factory|View
     */
    public function showResumeFormView()
    {
        if (!Auth::check()) {
            return abort(403);
        }

        $skills = \DB::table('skills')->get(['skillName']);
        return view('resumeForm', ['skills' => $skills]);
    }

    /**
     * register resume
     *
     * @param Resume $request
     * @return RedirectResponse
     */
    public function registerResume(Resume $request)
    {
        if (!Auth::check()) {
            abort(403);
        }
        $resumeOriginalFileName = $request->file('file')->getClientOriginalName();

        $resumeSavedFileName = $this->savedFileName($resumeOriginalFileName,
            $request->input('firstName'), $request->input('lastName'));

        $path = $this->UploadFile($request->file('file'), $resumeSavedFileName);
        if (!$path) {
            session()->flash("UPLOAD_ERROR", true);
            return back();
        }


        \DB::table('MYUSER')->insert([
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'resumeOriginalFileName' => $resumeOriginalFileName,
            'resumeSavedFileName' => $resumeSavedFileName,
            'user_id_at' => Auth::id()
        ]);

        $skills = $request->input('skill');

        $idUser = \DB::getPdo()->lastInsertId();

        foreach ($skills as $skill) {
            $idSkill = \DB::table('skills')
                ->where('skillName', '=', $skill)
                ->value('id');

            \DB::table('myuser_has_skills')->insert([
                'MYUSER_ID_MYUSER' => $idUser,
                'SKILLS_ID_SKILL' => $idSkill
            ]);
        }

        session()->flash("SUCCESS_REGISTER", $request->input('firstName') . ' ' . $request->input('lastName'));
        return back();
    }


    /**
     * generate new file name
     *
     * @param string $originalFileName
     * @param $firstName
     * @param $lastName
     * @return string
     */
    public function savedFileName(string $originalFileName, $firstName, $lastName): string
    {
        [$fileName, $fileExtension] = explode('.', $originalFileName);
        return $fileName . "-" . $firstName . $lastName . "." . $fileExtension;
    }

    /**
     * store file to Resume-file directory
     *
     * @param $file
     * @param $fileName
     * @return mixed
     */
    public function UploadFile($file, $fileName)
    {
        return $file->storeAs('Resume-File', $fileName);
    }

    /**
     * show resume detail by id
     *
     * @param $id
     * @return Factory|View
     */
    public function resumeShow($id)
    {
        if (!Auth::check()) {
            abort(403);
        }

        $resumes = \DB::table('MYUSER')
            ->select('*')
            ->where('user_id_at', '=', Auth::id())
            ->get();


        return view('resumesShow', compact(['resumes']));
    }

    public function singleResumeShow()
    {
        $userInf = $_GET['userInf'];

        $skillsId = DB::table('myuser_has_skills')
            ->select('SKILLS_ID_SKILL')
            ->where('MYUSER_ID_MYUSER', '=', $userInf['id'])
            ->get();

        foreach ($skillsId as $skillId) {

            $skills [] = DB::table('skills')
                ->select('skillName')
                ->where('id', '=', $skillId->SKILLS_ID_SKILL)
                ->get()
                ->first();
        }

        return view('singleResumeShow', compact(['skills', 'userInf']));
    }

    /**
     * download resume file
     *
     * @return BinaryFileResponse
     */
    public function downloadResumeFile()
    {
        $filePath = 'app' . DIRECTORY_SEPARATOR . $this->ResumeFileDirectory . DIRECTORY_SEPARATOR . $_GET['file'];
        $fileAbsolutePath = storage_path($filePath);

        return Response()->download($fileAbsolutePath);
    }
}
