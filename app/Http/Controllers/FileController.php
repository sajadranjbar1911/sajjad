<?php


namespace App\Http\Controllers;


use App\Comment;
use App\Events\NewFileUploaded;
use App\Files;
use App\Http\Requests\MyFile;
use App\Policies\FilePolicy;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FileController
{

    /**
     * @return Factory|View
     */
    public function showFormFile()
    {
        $tags = Tag::query()->get();
        if (Auth::check()) {
            return view('fileForm', ['tags' => $tags]);
        }
        return abort(403);
    }

    /**
     * store file
     * create
     *
     * @param MyFile $request
     * @return RedirectResponse
     */
    public function UploadFile(MyFile $request)
    {

        $originalFileName = $request->file('file')->getClientOriginalName();
        $savedFileName = $request->file('file')->hashName();
        $description = $request->input('description');

        $path = $request->file('file')->store('Upload-file');
        if (!$path)
            return 'upload error';

        session()->flash("UPLOAD", $originalFileName);

        $file = Files::query()->create([
            'originalFileName' => $originalFileName,
            'savedFileName' => $savedFileName,
            'description' => $description,
            'user_id' => Auth::id()
        ]);
        $inputTags = $request->input('tag');
        $tags = [];
        foreach ($inputTags as $tag) {
            $tags[$tag] = ['priority' => 3];
        }
        $file->tags()->sync($tags);

        return back();

    }

    /**
     * show form view for edit description
     *
     * @param Files $file
     * @return Factory|View|void
     */
    public function edit(Files $file)
    {
        return view('fileEdit', compact('file'));
    }

    /**
     * update description file
     * update
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateFile(Request $request)
    {
        $description = $request->input('description');
        $id = $_GET['fileId'];
        $result = Files::query()->where('id', '=', $id)->update(['description' => $description]);

        if ($result == 0) {
            session()->flash('UPDATE', 'Update_failed');
            return back();
        }

        session()->flash('UPDATE', 'Update_success');

        return back();

    }

    /**
     * read
     *
     * @param Request $request
     * @return Factory|View
     */
    public function showAllFile(Request $request)
    {


        $files = Files::query()
            ->when(Auth::check(), function (Builder $query) {
                $query->with(['comments', 'tags']);
            })
            ->when($request->has('has_comment'), function (Builder $query) {
                $query->whereHas('comments', function (Builder $query) {
                    $query->where('created_at', '>', Carbon::yesterday());
                });
            })
            ->when($request->has('has_tag'), function (Builder $query) {
                $query->with('tags')->oldest('priority');
            })
            ->when(!Auth::user()->is_admin, function (Builder $query) {
                $query->where('user_id', '=', Auth::id());
            })
            ->paginate(2);

        if ($files->isEmpty()) {
            return abort(404);
        }

        return view('showFile', ['files' => $files]);
    }

    /**
     * @param Files $file
     * @return Factory|View
     */
    public function show(Files $file)
    {
        return \view('showSingleFile', compact('file'));
    }

    /**
     * @param Files $file
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Files $file)
    {


        $result = $file->delete();
        if ($result == 0) {
            session()->flash('DELETE', "Delete_failed");
            return back();
        }

        $deleteFileResult = \Storage::delete('Upload-file/' . $file->savedFileName);
        if (!$deleteFileResult) {
            session()->flash('DELETE', "Delete_failed");
            return back();
        }

        session()->flash('DELETE', "Delete_success");
        return back();
    }

    /**
     * generate new name for save file
     *
     * @param string $originalFileName
     * @return string
     */
    public function savedFileName(string $originalFileName): string
    {
        [$fileName, $fileExtension] = explode('.', $originalFileName);
        return $fileName . "-" . rand(2, 100) . "." . $fileExtension;
    }

    /**
     * @param $fileId
     * @return Factory|View
     */
    public function Comment($fileId)
    {
        return \view('comment', compact('fileId'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function setComment(Request $request)
    {
        $fileId = $_GET['fileId'];
        Comment::query()->create([
            'text' => $request->comment,
            'File_id' => $fileId,
        ]);
        return response()->redirectTo('file/showFile');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function allComments(Request $request)
    {
        $comments = Comment::query()->with('commentable')
            ->when($request->has('only_file'), function (Builder $query) {
                $query->whereHasMorph('commentable_type', 'App\Files');
            })
            ->when($request->has('only_post'), function (Builder $query) {
                $query->whereHasMorph('commentable_type', 'App\Post');
            })
            ->latest()
            ->get();

        return \view('allComments', compact('comments'));
    }

    /**
     * @param $savedFileName
     * @return BinaryFileResponse
     */
    public function downloadFile($savedFileName)
    {
        return \Response::download(storage_path('app\Upload-file\\') . $savedFileName);
    }

    public function setTag()
    {
        Tag::created([

        ]);
    }

}
