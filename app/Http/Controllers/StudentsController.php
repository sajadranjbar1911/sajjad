<?php


namespace App\Http\Controllers;


use App\Imports\UsersImport;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class StudentsController
{

    private $ratios = [
        'e1' => 2,
        'e2' => 1,
        'e3' => 3,
        'e4' => 2,
        'e5' => 5,
        'e6' => 2,
        'e7' => 1,
        'e8' => 12,
    ];

    /**
     * read excel file of scores
     * creat text file for GPA
     * download and delete text file after download
     * @return BinaryFileResponse
     */
    public function downloadScores()
    {
        //Read the excel file of scores
        $scoresList = Arr::first(Excel::toArray(new UsersImport(), 'scores\\Scores.xlsx'));

        //delete exam of name
        unset($scoresList[0]);

        //CalculateGPA doing
        $GPAList = $this->calculateGPA($scoresList);

        //Creat text file for GPA list
        Storage::put("GPAList.txt", $GPAList);

        //download for user and delete file after download
        return Response()->download(storage_path("app\GPAList.txt"), "GPAList.txt")->deleteFileAfterSend(true);
    }

    /**
     * @param $scoresList
     * @return string
     */
    private function calculateGPA($scoresList)
    {
        $studentGPA = "";

        foreach ($scoresList as $student) {
            //get student of name
            $studentName = Arr::first($student);

            unset($student[0]);//unset student of name

            foreach ($student as $exam => $score) {
                //calculate score exam with ratios
                $scoreExam[$exam] = $score * $this->ratios["e" . $exam];
            }
            //creat text for write in text file
            $studentGPA .= " $studentName " . "=" . round(array_sum($scoreExam) / array_sum($this->ratios)) . "\r\n";
        }

        return $studentGPA;
    }

}
