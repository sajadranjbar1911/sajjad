<?php


namespace App\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Monolog\Handler\IFTTTHandler;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class ImageController
{


    /**
     * function for download image
     * @param $imageName
     * @param string $height
     * @param string $width
     * @return string
     */
    public function downloadImage($imageName, $height = null, $width = null)
    {


        // The existence of the image is checked
        if (!$this->imageExists($imageName))
            return Response()->json("NotFoundImage!", "404");

        // Set Height and checking Siz
        if (!empty($height) && !$this->checkedSiz($height)) {
            return "Height entered is out of range.Height should be between 50 and 150";
        }

        // Set Width and checking Siz
        if (!empty($width && !$this->checkedSiz($width))) {
            return "Width entered is out of range.Width should be between 50 and 150";
        }

        /*
         * If the length or width is entered, crop and download the photo,
         *otherwise it will download the original photo
         */
        if (isset($height) || isset($width)) {
            $image = $this->crop($imageName, $height, $width);//function for crop photo
            return Response()->download($image);
        } else {
            return Response()->download(storage_path("app\images\\$imageName"), $imageName);
        }


    }

    /**
     * function for checking size
     * @param $value
     * @return bool
     */
    private function checkedSiz($value)
    {
        return (50 <= $value && $value <= 150);
    }

    /**
     * function for The existence of the image is checked
     * @param $imageName
     * @return bool
     */
    private function imageExists($imageName)
    {
        return file_exists(storage_path("app\images\\$imageName"));
    }

    /**
     * function for The existence of the ResizedImage is checked
     * @param $imageBaseName
     * @param null $height
     * @param null $width
     * @return bool
     */
    private function imageResizedExists($imageBaseName, $height = null, $width = null)
    {
        //Get all resizedImage
        $allImage = scandir(storage_path("app\\resized-photos"));

        //The existence of the ResizedImage is checked
        foreach ($allImage as $imageResizedName) {

            if (strpos($imageResizedName, $imageBaseName) === 0) {
                $imgResizedName = Image::make(storage_path("app\\resized-photos\\$imageResizedName"));
                //Checking the length and width equality
                if ($imgResizedName->getHeight() == $height && $imgResizedName->getWidth() == $width)
                    return $imageResizedName;
            }

        }
        return false;
    }


    /**
     * function for Crop photo
     * @param $imageName
     * @param null $height
     * @param null $width
     * @return string
     */
    public function crop($imageName, $height = null, $width = null)
    {
        $imgName = explode(".", $imageName);
        $imageBaseName = reset($imgName);
        $imageType = end($imgName);

        //If there is a photo with the same name and size, no crop is done and the same photo is return
        $imageResizedName = $this->imageResizedExists($imageBaseName, $height, $width);
        if ($imageResizedName) {
            return storage_path("app\\resized-photos\\$imageResizedName");
        }

        //crop operation
        $image = Image::make(storage_path("app\images\\$imageName"));
        $imageResize = $image->resize($width, $height);
        //set new name
        $imageResizeName = $imageBaseName . "-" . rand(0, 1000) . "." . $imageType;
        //save resizedImage
        $imageResize->save(storage_path("app\\resized-photos\\$imageResizeName"));

        return storage_path("app\\resized-photos\\$imageResizeName");
    }
}
