<?php


namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogPost;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FormController extends Controller
{

    /**
     * @return Factory|View
     */
    public function showFormProfile()
    {
        return view('profileForm');
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function showDetail(StoreBlogPost $request)
    {
        \DB::table('MyUser')->insert([

                'firstName' => $request->input('firstName'),
                'lastName' => $request->input('lastName'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'username' => $request->input('username'),
                'password' => $request->input('password')
            ]
        );
    }


    /**
     * @return Factory|View
     */
    public function ShowChart()
    {
        $data = [
            ['id' => 1, 'name' => 'ali', 'username' => 'ali@gmail.com'],
            ['id' => 2, 'name' => 'masoud', 'username' => 'masoud@gmail.com'],
            ['id' => 3, 'name' => 'abas', 'username' => 'abas@gmail.com'],
        ];
        return view('arrayChart', ['data' => $data]);
    }

    /**
     * @return BinaryFileResponse
     */
    public function downloadChart()
    {
        $data = [
            ['id' => 1, 'name' => 'ali', 'username' => 'ali@gmail.com'],
            ['id' => 2, 'name' => 'masoud', 'username' => 'masoud@gmail.com'],
            ['id' => 3, 'name' => 'abas', 'username' => 'abas@gmail.com']
        ];

        Storage::put('get-user-list-html/html.txt', view('arrayChart', ['data' => $data]));
        return response()->download(storage_path("app/get-user-list-html/html.txt"))->deleteFileAfterSend();
    }

    /**
     * @return Factory|View
     */
    public function showUser()
    {
        $users = \DB::table('users')->paginate(15);
//        return $users; //return a json
        return view('showUser', ['users' => $users]);

    }


}
