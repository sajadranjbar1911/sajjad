<?php

namespace App\Http\Requests;

use App\Rules\EasyPassword;
use App\Rules\StrongPassword;
use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'firstName' => ['required', 'alpha', 'min:2', 'max:60', 'nullable'],
            'lastName' => ['required', 'alpha', 'min:2', 'max:60'],
            'email' => ['required', 'E-Mail'],
            'phone' => ['required'],
            'username' => ['required'],
            'password' => ['required', new StrongPassword($this->input('firstName'), $this->input('lastName')
                , $this->input('email'), $this->input('username'))]
        ];
    }
}
