<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MyFile extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required', 'mimes:pdf,zip', 'max:5000'],
            'description'=>['required', 'alpha_dash']
        ];
    }
}
