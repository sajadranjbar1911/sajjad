<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Resume extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => ['required', 'string', 'min:2', 'max:30'],
            'lastName' => ['required', 'string', 'min:2', 'max:30'],
            'skill' => ['required', 'array', 'min:3'],
            'skill.*' => [Rule::in(['PHP', 'JAVA', 'PYTHON', 'JAVASCRIPT', 'MATLAB', 'C/C++', 'Swift', 'C#', 'GO'])],
            'phone' => ['nullable', 'numeric', 'digits:11'],
            'email' => ['nullable', 'email'],
            'file' => ['required', 'mimes:pdf', 'max:5000']
        ];
    }
}
