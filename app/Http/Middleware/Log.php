<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;

class Log
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        [$controller, $method] = explode('@', \request()->route()->getAction('controller'));

        \App\Log::query()->create([
            'ip' => $request->getClientIp(),
            'user_agent' => $request->header('User-Agent'),
            'controller+action' => $controller . '+' . $method,
            'created_at' => date("Y/m/d h:i:s"),
            'user_id' => \Auth::id()
        ]);
        return $next($request);
    }
}
