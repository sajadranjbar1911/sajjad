<?php

namespace App\Listeners;

use App\Events\NewFileUploaded;
use App\Files;
use App\Jobs\ExtractZip;
use App\Jobs\ExtractZipJob;
use App\Jobs\TestJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use mysql_xdevapi\Exception;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class CheckZipFile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NewFileUploaded $event
     * @return void
     * @throws \Exception
     */
    public function handle(NewFileUploaded $event)
    {
        $extension = pathinfo($event->file->originalFileName, PATHINFO_EXTENSION);
        try {
            if ($extension === 'zip') {

                dispatch(new ExtractZipJob($event->file));
            }

        } catch (\Exception $e) {
            \Log::error($e);
            return null;
        }
    }
}
