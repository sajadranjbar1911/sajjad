<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $table = 'log';

    public $timestamps = false;


    protected $fillable = ['ip', 'user_agent', 'controller+action', 'created_at', 'user_id'];
}
