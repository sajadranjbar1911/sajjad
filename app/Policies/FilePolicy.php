<?php

namespace App\Policies;

use App\Files;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any files.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the files.
     *
     * @param \App\User $user
     * @param \App\Files $files
     * @return mixed
     */
    public function view(User $user, Files $files)
    {
        return (empty($files) || $user->is_admin);
    }

    /**
     * Determine whether the user can create files.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return ($user->is_admin);
    }

    /**
     * Determine whether the user can update the files.
     *
     * @param \App\User $user
     * @param \App\Files $files
     * @return mixed
     */
    public function update(User $user, Files $file)
    {
        return (!empty($file) || !$user->is_admin);
    }

    /**
     * Determine whether the user can delete the files.
     *
     * @param \App\User $user
     * @param \App\Files $files
     * @return mixed
     */
    public function delete(User $user, Files $file)
    {
        return ($file->user_id == $user->id || $user->is_admin);
    }

    /**
     * Determine whether the user can restore the files.
     *
     * @param \App\User $user
     * @param \App\Files $files
     * @return mixed
     */
    public function restore(User $user, Files $files)
    {
        return (empty($files) || $user->is_admin);
    }

    /**
     * Determine whether the user can permanently delete the files.
     *
     * @param \App\User $user
     * @param \App\Files $files
     * @return mixed
     */
    public function forceDelete(User $user, Files $files)
    {
        return (empty($files) || $user->is_admin);
    }
}
