<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = 'tag';

    protected $fillable = ['name'];

    public function files()
    {
        return $this->morphedByMany(Files::class,
            'taggble',
            'taggables_tag',
            'tag_id',
            'taggble_id')
            ->withPivot('priority');
    }

    public function posts()
    {
        return $this->morphedByMany(
            Post::class,
            'taggble',
            'taggables_tag',
            'tag_id',
            'taggble_id'
        )->withPivot('priority');
    }

}
