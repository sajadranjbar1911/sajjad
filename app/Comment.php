<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'comment';

    protected $fillable = ['text', 'type_id', 'type'];

    public function commentable()
    {
        return $this->morphTo('commentable', 'commentable_type', 'commentable_id');
    }

}
