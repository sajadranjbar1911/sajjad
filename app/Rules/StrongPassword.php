<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class StrongPassword implements Rule
{


    private $input;

    public function __construct($firstName, $lastName, $email, $username)
    {
        $this->input = ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email, 'username' => $username];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $input = array_filter($this->input, function ($arr) {
            if (!empty($arr))
                return true;
        });
        return !Str::contains($value, $input);

    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('validation.StrongPassword');
    }
}

