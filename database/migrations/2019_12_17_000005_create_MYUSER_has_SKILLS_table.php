<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyuserHasSkillsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'MYUSER_has_SKILLS';

    /**
     * Run the migrations.
     * @table MYUSER_has_SKILLS
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('MYUSER_ID_MYUSER');
            $table->unsignedInteger('SKILLS_ID_SKILL');

            $table->index(["SKILLS_ID_SKILL"], 'fk_MYUSER_has_SKILLS_SKILLS1_idx');

            $table->index(["MYUSER_ID_MYUSER"], 'fk_MYUSER_has_SKILLS_MYUSER_idx');


            $table->foreign('MYUSER_ID_MYUSER', 'fk_MYUSER_has_SKILLS_MYUSER_idx')
                ->references('ID_MYUSER')->on('MYUSER')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('SKILLS_ID_SKILL', 'fk_MYUSER_has_SKILLS_SKILLS1_idx')
                ->references('ID_SKILL')->on('SKILLS')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
