<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileHasTagTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'File_has_tag';

    /**
     * Run the migrations.
     * @table File_has_tag
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('File_id');
            $table->unsignedInteger('tag_id');

            $table->integer('priority')->default('1');

            $table->primary(['File_id', 'tag_id']);

            $table->index(["tag_id"], 'fk_File_has_tag_tag1_idx');

            $table->index(["File_id"], 'fk_File_has_tag_File1_idx');


            $table->foreign('File_id', 'fk_File_has_tag_File1_idx')
                ->references('id')->on('File')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('tag_id', 'fk_File_has_tag_tag1_idx')
                ->references('id')->on('tag')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
