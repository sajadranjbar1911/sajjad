<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIdAtColumn extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'MYUSER';

    /**
     * Run the migrations.
     * @table MYUSER
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->integer('user_id_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn('user_id_at');
        });
    }
}
